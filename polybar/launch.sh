#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

MONITOR=eDP1 polybar --reload top &
sleep 1

MONITOR=HDMI1 polybar --reload top &
MONITOR=DP1 polybar --reload top &
# if type "xrandr"; then
#   for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
#     MONITOR=$m polybar --reload top &
#   done
# else
#   polybar --reload top &
# fi
#
