#!/bin/bash

choices="vgaOff\nhdmiOff\nvgaRight\nvgaLeft\nhdmi"

chosen=$(echo -e "$choices" | dmenu -i)

case "$chosen" in
	vgaOff)xrandr --output eDP1 --auto --output DP1 --off && $HOME/.config/polybar/launch.sh
;;
	
	hdmiOff)xrandr --output eDP1 --auto --output HDMI1 --off && $HOME/.config/polybar/launch.sh
;;
	vgaRight) xrandr --output  eDP1 --auto --output DP1 --auto --right-of eDP1 && $HOME/.config/polybar/launch.sh && nitrogen --restore;;

	vgaLeft) xrandr --output  eDP1 --auto --output DP1 --primary --auto --left-of eDP1 && $HOME/.config/polybar/launch.sh && nitrogen --restore;;
	
	hdmi) xrandr --output  eDP1 --auto --output HDMI1 --auto --right-of eDP1 && $HOME/.config/polybar/launch.sh;;

esac
