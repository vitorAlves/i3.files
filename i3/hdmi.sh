#!/bin/bash
EXTERNAL_OUTPUT="HDMI1"
INTERNAL_OUTPUT="eDP1"



if [ ! -f "/tmp/monitor_mod.dat" ] ; then

  monitor_mode="all"

# otherwise read the value from the file
else
  monitor_mode=`cat /tmp/monitor_mod.dat`
fi
   

if [ $monitor_mode = "all" ]; then
        monitor_mode="laptop"
        xrandr --output $INTERNAL_OUTPUT --auto --output $EXTERNAL_OUTPUT --off

elif [ $monitor_mode = "laptop" ]; then
        
	monitor_mode="all"
        xrandr --output $INTERNAL_OUTPUT --auto --output $EXTERNAL_OUTPUT --auto --right-of $INTERNAL_OUTPUT

fi
echo "${monitor_mode}" > /tmp/monitor_mod.dat
